/*
 * https://www.w3resource.com/c-programming-exercises/function/c-function-exercise-2.php
 */

#include <stdio.h>

double square(double num)
{
    return (num * num);
}
int main()
{
    float num;
    double n;
    printf("\n\n Function : find square of any number :\n");
    printf("------------------------------------------------\n");

    printf("Input any number for square : ");
    scanf("%f", &num);
    n = square(num);
    printf("The square of %f is : %.2f\n", num, n);
    return 0;
}
