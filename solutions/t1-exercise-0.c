/*
 * https://www.w3resource.com/c-programming-exercises/basic-declarations-and-expressions/c-programming-basic-exercises-3.php
 */


#include <stdio.h>
int main()
{
    printf("    ######\n");
    printf("  ##      ##\n");
    printf(" #\n");
    printf(" #\n");
    printf(" #\n");
    printf(" #\n");
    printf(" #\n");
    printf("  ##      ##\n");
    printf("    ######\n");

    return(0);
}
