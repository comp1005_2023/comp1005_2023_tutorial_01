COMP1005 2023 Tutorial 1
========================


## Background

* [Hello World](https://www.learn-c.org/en/Hello%2C_World%21)
* [Variables and Types](https://www.learn-c.org/en/Variables_and_Types)
* [Functions](https://www.learn-c.org/en/Functions)
* [Input and Output](https://www.tutorialspoint.com/cprogramming/c_input_output.htm)
* [Printing](https://www.codingunit.com/printf-format-specifiers-format-conversions-and-formatted-output)
* [Operators](https://www.tutorialspoint.com/cprogramming/c_operators.htm)
    * [Arithmetic Operators](https://www.tutorialspoint.com/cprogramming/c_arithmetic_operators.htm)
    * [Relational Operators](https://www.tutorialspoint.com/cprogramming/c_relational_operators.htm)
    * [Bitwise Operators](https://www.tutorialspoint.com/cprogramming/c_bitwise_operators.htm)

## Exercises

0. Write a C program to print a big 'C'.
    ***Expected Output:***
    ```
        ######
      ##      ##
     #
     #
     #
     #
     #
      ##      ##
        ######
    ```

0. Write a C program to compute the perimeter and area of a rectangle with a height of 7 inches. and width of 5 inches.
    ***Expected Output:***
    ```
    Perimeter of the rectangle = 24 inches
    Area of the rectangle = 35 square inches
    ```

0. Write a C program to display multiple variables.
    ***Declaration :***
    ```
    int a = 125, b = 12345;
    long ax = 1234567890;
    short s = 4043;
    float x = 2.13459;
    double dx = 1.1415927;
    char c = 'W';
    unsigned long ux = 2541567890;
    ```
    ***Expected Output:***
    ```
    a + c =  212
    x + c = 89.134590
    dx + x = 3.276183
    ((int) dx) + ax =  1234567891
    a + x = 127.134590
    s + b =  16388
    ax + b = 1234580235
    s + c =  4130
    ax + c = 1234567977
    ax + ux = 3776135780
    ```

0. Write a C program that accepts an integer as the number of days.
Convert the days into years (*ignore leap year.*), weeks and days.
    ***Expected Output:***
    ```
    Please input the number of days: 1329
    Years: 3
    Weeks: 33
    Days: 3
    ```

0. Write a program in C to calculate the square of a given number using a function.
    ***Expected Output:***
    ```
    Input any number for square : 3.5
    The square of 3.50 is : 12.25
    ```


## Extras
0. Write a C program to read an amount (integer value) and break the amount into smallest possible number of bank notes using function.
    ***Expected Output:***
    ```
    Input the amount: 375
    There are:
    3 Note(s) of 100.00
    1 Note(s) of 50.00
    1 Note(s) of 20.00
    0 Note(s) of 10.00
    1 Note(s) of 5.00
    0 Note(s) of 2.00
    0 Note(s) of 1.00
    ```

0. Write a program in C to swap two numbers using function.
    ***Expected Output:***
    ```
    Input 1st number : 2
    Input 2nd number : 4
    Before swapping: n1 = 2, n2 = 4
    After swapping: n1 = 4, n2 = 2
    ```

0. Try the printf function with different 'Format Specifiers'.
